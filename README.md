**Montaje e instalacion**

1.El programa funciona con una app.js la cual no es necesario abrir,solamente hay que abrir el archivo index.html y correra.
(Fue iniciado con Vue, mas no es necesaria su instalacion)

2.Abrir archivo index.html

3.Jugar con la sopa de letras.


**Logica y funcionamiento**

1.Se realiza el html, en el cual ira la parte visual como es el INPUT, los BUTTONS y la LISTA y el cuadro de la matriz.
Finalmente se agrega el SCRIPT donde ira la logica del programa.

2.En la parte del SCRIPT, el programa realiza la logica de la siguiente manera:
	
	1.Se crea una DATA, las variables, un arreglo con el ABECEDARIO y una matriz vacia la cual usaremos despues
	aparte se crea un arreglo con las posiciones para que este funcione de diferentes formas.
	
	2.Se usa la funcion Addwords, para que haga un push a las letras que ingresa la persona y se condiciona en 
	caso de que sean muchas letras.
	
	3.Se usa la funcion generateRow para generar filas y poblar la matriz, adicional a esto con la funcion 
	Math.random(muy importante para el programa) hacemos que nos devuelva una posicion aleatoria.
	
	4.Condicionamos el programa para que nos devuelva filas aleatorias y haga un Split(fracciona la palabra LETRA POR 
	LETRA y lo devuelve como un arreglo.
	
	5.Se crea una funcion llamada generateValidIndex, la cual tendra un Math.random y se usara en todas las posiciones
	cambiando unicamente el orden de las columnas y las filas.
	
	6.Dentro de cada codicional va un for, el cual funciona para que las letras salgan en MAYUSCULA y se pinten de un 
	color primario en este caso AZUL.
	
	7.En la ultima condicional que es la DIAGONAL INVERTIDA,se usa un do-while donde sear menor o igual que times, para
	que este funcione sin problmea.

Dentro del codigo van coemntadas las funciones y algunos datos para que sea mas entendible el programa.

Espero les guste y puedan disfrutarlo.


