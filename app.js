new Vue({
  el: "#app",
  data: {
    word: "",
    words: ["Prueba", "Felipe", "Perro", "Gato", "Programa"],
    abecedario: [
      "a",
      "b",
      "c",
      "d",
      "e",
      "f",
      "g",
      "h",
      "i",
      "j",
      "k",
      "l",
      "m",
      "n",
      "o",
      "p",
      "q",
      "r",
      "s",
      "t",
      "u",
      "v",
      "w",
      "x",
      "y",
      "z",
    ],
    matriz: [],
    times: 15,
    positions: [
      "rl", // Derecha Izquerda
      "lr", // Izquerda Derecha
      "ud", // Arriba Abajo
      "du", // Abajo Arriba
      "vrlu", // Vertical Derecha Izquierda Arriba
      "vrld", // Vertical Derecha Izquierda Abajo
      "vlru", // Vertical Izquierda Derecha Arriba
      "vlrd", // Vertical Izquierda Derecha Abajo
    ],
  },
  mounted() {
    this.generatePuzle();
  },
  methods: {
    addWord() {
      if (this.word.length <= this.times) {
        this.words.push(this.word); // Agrega la palabra al arreglo
        this.word = ""; // Se resetea el valor
      } else {
        alert(`La palabra no puede tener mas de ${this.times} letras`);
      }
    },
    generatePuzle() {
      this.matriz = [];

      for (let i = 0; i < this.times; i++) {
        let row = this.generateRow(); // Generar las filas
        this.matriz.push(row); // Popular la matriz
      }

      for (const word of this.words) {
        let indexPosition = parseInt(Math.random() * this.positions.length);
        let positionRandom = this.positions[indexPosition]; // Devuleve una posicion aleatoria

        console.log(positionRandom);

        let wordLength = word.length; // Cantidad de letras de la palabra

        let indexColumn = 0;
        let indexRow = 0;

        if (positionRandom === "lr" || positionRandom === "rl") {
          indexRow = parseInt(Math.random() * this.matriz.length); // Devuelve el index de una fila aleatoria
          indexColumn = this.generateValidIndex(
            indexRow,
            indexColumn,
            wordLength
          );

          let splitWord = word.split(""); // Hacemos un split a la palabra, esto devulve un arreglo

          if (positionRandom === "rl") {
            splitWord.reverse();
          }

          for (const [i, letter] of splitWord.entries()) {
            this.matriz[indexRow][indexColumn + i] = {
              val: letter.toUpperCase(),
              bg: "primary",
            };
          }
        }

        if (positionRandom === "ud" || positionRandom === "du") {
          indexColumn = parseInt(Math.random() * this.matriz.length); // Devuelve el index de una fila aleatoria
          indexRow = this.generateValidIndex(indexRow, indexColumn, wordLength);

          let splitWord = word.split(""); // Hacemos un split a la palabra, esto devulve un arreglo

          if (positionRandom === "du") {
            splitWord.reverse();
          }

          for (const [i, letter] of splitWord.entries()) {
            this.matriz[indexRow + i][indexColumn] = {
              val: letter.toUpperCase(),
              bg: "primary",
            };
          }
        }

        if (positionRandom === "vlrd" || positionRandom === "vlru") {
          if (positionRandom === "vlrd") {
            indexRow = parseInt(Math.random() * this.matriz.length); // Devuelve el index de una fila aleatoria
            indexColumn = this.generateValidIndex(
              indexRow,
              indexColumn,
              wordLength
            );
            indexRow = this.generateValidIndex(
              indexColumn,
              indexRow,
              wordLength
            );
          } else if (positionRandom === "vlru") {
            indexColumn = parseInt(Math.random() * this.matriz.length); // Devuelve el index de una fila aleatoria
            indexRow = this.generateValidIndex(
              indexRow,
              indexColumn,
              wordLength
            );
            indexColumn = this.generateValidIndex(
              indexColumn,
              indexRow,
              wordLength
            );
          }

          let splitWord = word.split(""); // Hacemos un split a la palabra, esto devulve un arreglo

          if (positionRandom === "vlru") {
            splitWord.reverse();
          }

          for (const [i, letter] of splitWord.entries()) {
            this.matriz[indexRow + i][indexColumn + i] = {
              val: letter.toUpperCase(),
              bg: "primary",
            };

            console.log(this.matriz[indexRow + i][indexColumn + i]);
          }
        }

        if (positionRandom === "vrld" || positionRandom === "vrlu") {
          indexRow = parseInt(Math.random() * this.matriz.length);
          indexRow = this.generateValidIndex(indexColumn, indexRow, wordLength);

          do {
            indexColumn = parseInt(
              Math.random() * this.matriz[indexRow].length
            );
          } while (indexColumn + wordLength <= this.times);

          let splitWord = word.split(""); // Hacemos un split a la palabra, esto devulve un arreglo

          if (positionRandom === "vrlu") {
            splitWord.reverse();
          }

          for (const [i, letter] of splitWord.entries()) {
            this.matriz[indexRow + i][indexColumn - i] = {
              val: letter.toUpperCase(),
              bg: "primary",
            };

            console.log(this.matriz[indexRow + i][indexColumn - i]);
          }
        }
      }
    },
    generateValidIndex(positionA, positionB, wordLength) {
      let index = positionB;
      do {
        index = parseInt(Math.random() * this.matriz[positionA].length); // Devuelve el index de una columna aleatoria, de la fila anterior
      } while (index + wordLength > this.times); // Haga esto hasta que se genera un numero valido

      return index;
    },
    generateRow() {
      let row = [];

      for (let i = 0; i < this.times; i++) {
        let index = parseInt(Math.random() * this.abecedario.length);
        let letter = this.abecedario[index].toUpperCase();
        row.push(letter);
      }

      return row;
    },
  },
});
